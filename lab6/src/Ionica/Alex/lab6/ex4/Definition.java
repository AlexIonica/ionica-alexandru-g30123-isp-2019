package Ionica.Alex.lab6.ex4;


public class Definition {
    private String description;

    String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    Definition(String description) {
        this.description = description;
    }

}