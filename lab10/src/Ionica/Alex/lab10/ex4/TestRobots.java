package Ionica.Alex.lab10.ex4;

public class TestRobots{

    static public Robot Robots[] = new Robot[10];

    public static void main(String[] args){
        Robots[0] = new Robot("Robot1",0,0);
        Robots[1] = new Robot("Robot2",0,0);
        Robots[2] = new Robot("Robot3",0,0);
        Robots[3] = new Robot("Robot4",0,0);
        Robots[4] = new Robot("Robot5",0,0);
        Robots[5] = new Robot("Robot6",0,0);
        Robots[6] = new Robot("Robot7",0,0);
        Robots[7] = new Robot("Robot8",0,0);
        Robots[8] = new Robot("Robot9",0,0);
        Robots[9] = new Robot("Robot10",0,0);
        Plansa p = new Plansa();
        Robots[0].start();
        Robots[1].start();
        Robots[2].start();
        Robots[3].start();
        Robots[4].start();
        Robots[5].start();
        Robots[6].start();
        Robots[7].start();
        Robots[8].start();
        Robots[9].start();
    }
}

