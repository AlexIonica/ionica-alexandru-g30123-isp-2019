package Ionica.Alex.lab4.ex4;

import Ionica.Alex.lab4.ex2.Author;



class Book2 extends Author {

    private String name;
    private double price;
    private Author[] author;
    private int qtyInStock = 0;

    public Book2(String name, Author[] authors, double price) {
        this.author = authors;
        this.name = name;
        this.price = price;
    }

    public Book2(String name, Author[] authors, double price, int qtyInStock) {
        this.author = authors;
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Author[] getAuthors() {
        return author;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        String str = "Book called: " + this.name + " by " + this.getAuthors().length;
        return str;
    }

    public void printAuthors() {
        Author[] authors1 = getAuthors();
        for (int i = 0; i < authors1.length; i++) System.out.println(authors1[i].getName());
    }
}
