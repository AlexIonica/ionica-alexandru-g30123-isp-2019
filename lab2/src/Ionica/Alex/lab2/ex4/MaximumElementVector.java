package Ionica.Alex.lab2.ex4;

import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;


public class MaximumElementVector {


    public static void main(String[] args) {

        Scanner num = new Scanner(System.in);
        Vector<Double> v = new Vector<Double>();
        System.out.println("Give the numbers!");
        for(int i=0;i<5;i++)
        {
            System.out.println("x["+i+"]= ");
            double x = num.nextInt();
            v.add(new Double(x));
        }
        Object obj = Collections.max(v);
        System.out.println(obj);

        /*
        public class MaximumElementVector {


        public static void main(String[] args) {

            Vector<Double> v = new Vector<Double>();

            v.add(new Double("3"));
            v.add(new Double("5"));
            v.add(new Double("17"));
            v.add(new Double("-2"));
            v.add(new Double("2"));

            Object obj = Collections.max(v);
            System.out.println(obj);
        }


    }
         */
    }


}