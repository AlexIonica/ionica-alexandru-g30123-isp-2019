package Ionica.Alex.lab5.ex3;

import java.util.concurrent.ThreadLocalRandom;

public class TemperatureSensor extends Sensor {
    public static int tempSensor;

    public static int getTempSensor() {
        tempSensor = ThreadLocalRandom.current().nextInt(0, 101);
        return tempSensor;
    }
}
