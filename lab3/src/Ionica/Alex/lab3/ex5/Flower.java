package Ionica.Alex.lab3.ex5;


public class Flower {
    int petal;
    static int count = 0;

    Flower() {
        System.out.println("Flower has been created!");
        count++;
    }

    public static int getCount() {
        return count;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("It was created " + getCount() + " objects.");
    }
}