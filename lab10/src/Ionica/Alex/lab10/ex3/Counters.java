package Ionica.Alex.lab10.ex3;

public class Counters extends Thread {

    int counter;
    int valueToCount;
    int count = 0;
    Thread t;

    public Counters(int valueToCount,int counter, int countStart,Thread thread){
        this.valueToCount = valueToCount;
        this.counter = counter;
        this.count = countStart;
        this.t=thread;
    }

    public static void main(String[] args) {
        Counters c1 = new Counters(100,1,0,null);
        Counters c2 = new Counters(200,2,100,c1);
        c1.start();
        c2.start();
    }

    public void run() {
        System.out.println("Counter:" + counter);
        try{
            if(t!=null) t.join();
            while(count< valueToCount){
                count++;
                System.out.println(counter+":"+count);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}