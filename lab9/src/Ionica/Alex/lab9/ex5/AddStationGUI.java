package Ionica.Alex.lab9.ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddStationGUI extends JFrame implements ActionListener {

    JTextField numeStatie;
    JLabel nume;
    JButton Cancel, OK;
    Controler controler;

    public AddStationGUI() {
        setTitle("Add a station.");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 120);
        setResizable(false);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);

        nume = new JLabel("Introduceti numele:");
        nume.setBounds(30, 10, 120, 20);

        numeStatie = new JTextField();
        numeStatie.setBounds(150, 10, 200, 20);

        Cancel = new JButton("Cancel");
        Cancel.setBounds(180, 40, 80, 20);

        OK = new JButton("OK");
        OK.setBounds(270, 40, 80, 20);

        Cancel.addActionListener(this);
        OK.addActionListener(this);

        add(OK);
        add(Cancel);
        add(nume);
        add(numeStatie);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (((JButton) e.getSource()).getText().equals("OK"))
            if (numeStatie.getText().equals("")) {
                System.out.println(numeStatie.getText());
                new HandleError(1);
            } else {
                controler = new Controler(numeStatie.getText());
                SimulatorGUI.logContentSetter("Controller nou creat:" + numeStatie.getText());
                SimulatorGUI.addController(controler);
                new AddSegmentGUI(controler);
                dispose();
            }
        if (((JButton) e.getSource()).getText().equals("Cancel")) dispose();
    }
}