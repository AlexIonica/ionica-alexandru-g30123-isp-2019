package Ionica.Alex.lab6.ex2;

public class Main {
    public static void main(String[] args) {
        Bank b = new Bank();
        BankAccount b1 = new BankAccount("owner1", 30);
        b1.withdraw(50);
        b1.deposit(150);
        b1.withdraw(70);

        b.addAccount("Alex", 150);
        b.addAccount("Mihai", 200);
        b.addAccount("Andrei", 60);

        b.printAccounts(150, 200);
        b.printAccounts();
        b.getAllAccounts();
    }
}