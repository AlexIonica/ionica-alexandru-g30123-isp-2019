package Ionica.Alex.lab7.ex1;

class CofeeMaker {
    static int cofeeCount=0;
    int LIMIT=5;
    Cofee makeCofee() throws MakeException{
        if(cofeeCount>=LIMIT){
            throw(new MakeException("Too much cofee made"));
        }
        System.out.println("Make a coffe");
        cofeeCount++;
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Cofee cofee = new Cofee(t,c);
        return cofee;
    }

}//.class
