package Ionica.Alex.lab4.ex6;

public class Circle extends Shape {
    private double radius = 1.0;
    private String color = "red";
    private static final float PI = 3.1415f;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public String toString() {
        System.out.println("A shape with color " + super.color + " and " + super.filled);
        return null;
    }
}
