package Ionica.Alex.lab10.ex4;

import java.util.ArrayList;

public class Plansa{

    int dimX = 100;
    int dimY = 100;
    static int maxDim = TestRobots.Robots.length;
    static ArrayList<Robot> FlaggedRobots = new ArrayList<Robot>();
    static int current = 0;

    public static void checkCollision(){
        boolean ok = true;
        for(int i = 0; i< maxDim; i++) {
            for (int j = 0; j < maxDim; j++) {
                if (i != j) {
                    if ((TestRobots.Robots[i].PosX == TestRobots.Robots[j].PosX) && (TestRobots.Robots[i].PosY == TestRobots.Robots[j].PosY)) {
                        for (Robot currentRobot : FlaggedRobots) {
                            if (TestRobots.Robots[i].equals(currentRobot)) ok = false;
                            if (TestRobots.Robots[j].equals(currentRobot)) ok = false;
                        }

                        if (ok == true) {
                            System.out.println(TestRobots.Robots[i].nume + " collided with " + TestRobots.Robots[j].nume + " and both exploded.");
                            TestRobots.Robots[i].stop();
                            TestRobots.Robots[j].stop();
                            FlaggedRobots.add(TestRobots.Robots[i]);
                            FlaggedRobots.add(TestRobots.Robots[j]);
                        }
                    }
                }
            }
        }
    }
}

