package Ionica.Alex.lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {

    static float presses = 0;
    static int number = 0;
    static float cps = 0;
    static JLabel counter;
    static JLabel lCps;
    static JTextArea count;
    static JTextArea tCps;
    JButton doSmt;
    static int width = 80;
    static int height = 20;

    private static boolean running = true;

    Counter() {
        setTitle("Counter program.");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(180, 200);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);

        counter = new JLabel("Counter:");
        counter.setBounds(10, 20, width, height);

        count = new JTextArea();
        count.setBounds(70, 20, width, height);
        count.setEnabled(false);
        count.setText(String.valueOf(number));

        lCps = new JLabel("Clicks/sec:");
        lCps.setBounds(10, 50, width, height);

        tCps = new JTextArea();
        tCps.setBounds(80, 50, width - 10, height);
        tCps.setEnabled(false);
        tCps.setText(String.valueOf(cps));

        doSmt = new JButton("Press");
        doSmt.setBounds(10, 100, 145, 50);
        doSmt.addActionListener(new doSmtOnPress());


        add(counter);
        add(count);
        add(doSmt);
        add(lCps);
        add(tCps);
    }

    public static void main(String[] args) {
        new Counter();
        run();
    }

    public static void run() {
        float clicks = 0;
        double uSec = 0;
        double secPerTick = 1 / 60.0;
        int countTick = 0;
        long prevTime = System.nanoTime();

        while (running) {
            long currTime = System.nanoTime();
            long pasTime = currTime - prevTime;
            prevTime = currTime;
            uSec += pasTime / 1000000000.0;

            while (uSec > secPerTick) {
                uSec -= secPerTick;
                countTick++;
                if (countTick % 60 == 0) {
                    tCps.setText(String.valueOf(clicks));
                    cps = clicks;
                    clicks = 0;
                    presses = 0;
                }
            }
            clicks = presses;
        }
    }
}

class doSmtOnPress implements ActionListener {


    @Override
    public void actionPerformed(ActionEvent e) {
        Counter.presses++;
        Counter.number++;
        Counter.count.setText(null);
        Counter.count.setText(String.valueOf(Counter.number));
    }
}