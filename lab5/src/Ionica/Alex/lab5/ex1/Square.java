package Ionica.Alex.lab5.ex1;

public class Square extends Rectangle{
    protected double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled){
        super(color,filled,side,side);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double side){
        super.width=side;
    }

    public void setLength(double side){
        super.length=side;
    }

    public String toString(){
        String str = "A square with an area of:"+super.getArea()+" and a perimeter of: "+super.getPerimeter()+" is filled:"+super.isFilled()+" and is colored: "+super.getColor();
        return str;
    }
}