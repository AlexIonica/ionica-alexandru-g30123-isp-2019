package Ionica.Alex.lab6.ex2;

import java.util.Collections;

import java.util.ArrayList;


public class Bank {

    private ArrayList<BankAccount> bankaccount = new ArrayList<>();

    void addAccount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        bankaccount.add(b);
    }

    void printAccounts() {

        System.out.print(bankaccount.toString());

    }

    void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount ba : bankaccount) {
            if (ba.getBalance() > minBalance) {
                if (ba.getBalance() < maxBalance) {
                    System.out.println(ba.getOwner());
                }
            }
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount ba : bankaccount) {
            if (ba.getOwner().equals(owner)) {
                return ba;
            }
        }
        return null;
    }

    void getAllAccounts() {
        Collections.sort(bankaccount);

        System.out.print(bankaccount.toString());

    }
}