package Ionica.Alex.lab9.ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddSegmentGUI extends JFrame implements ActionListener {

    JLabel nrSegmenteL;
    JTextField nrSegmenteT;
    JTextField[] segmentInput;
    JButton OKNrSeg, Done;
    int nr;
    Controler cont;


    public AddSegmentGUI(Controler c) {
        setTitle("Add segments to: " + c.toString());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setResizable(false);
        setVisible(true);
        cont = c;
    }

    private void init() {
        this.setLayout(null);

        setSize(400, 120);
        nrSegmenteL = new JLabel("Nr. segmente:");
        nrSegmenteL.setBounds(30, 10, 120, 20);
        nrSegmenteT = new JTextField();
        nrSegmenteT.setBounds(120, 10, 20, 20);
        OKNrSeg = new JButton("SET");
        OKNrSeg.setBounds(160, 10, 80, 20);
        OKNrSeg.addActionListener(this);

        add(OKNrSeg);
        add(nrSegmenteL);
        add(nrSegmenteT);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (((JButton) e.getSource()).getText().equals("SET")) {
            OKNrSeg.setEnabled(false);
            nrSegmenteT.setEnabled(false);

            nr = Integer.parseInt(nrSegmenteT.getText());
            segmentInput = new JTextField[nr];
            JLabel[] label = new JLabel[nr];

            setSize(400, 150 + nr * 40);
            for (int i = 0; i < nr; i++) {
                segmentInput[i] = new JTextField();
                label[i] = new JLabel("ID Seg" + (i + 1) + ":");
                segmentInput[i].setBounds(80, (i + 1) * 40, 300, 30);
                label[i].setBounds(10, (i + 1) * 40, 80, 30);

                add(segmentInput[i]);
                add(label[i]);
            }

            Done = new JButton("Done");
            Done.setBounds(270, nr * 40 + 80, 80, 20);
            add(Done);
            Done.addActionListener(this);
        }

        if (((JButton) e.getSource()).getText().equals("Done")) {
            System.out.println("test");
            Segment[] seg = new Segment[nr];

            boolean crap = false;
            for (int i = 0; i < nr; i++) {
                if (segmentInput[i].getText().equals("")) {
                    new HandleError(2);
                    crap = true;
                    break;
                }
            }

            if (crap == true) return;

            for (int i = 0; i < nr; i++) {
                seg[i] = new Segment(Integer.parseInt(segmentInput[i].getText()));
                cont.addControlledSegment(seg[i]);
                SimulatorGUI.addSegment(seg[i]);
                SimulatorGUI.logContentSetter("Segment creat:" + seg[i].id + " pe controllerul:" + cont.stationName);
            }
            dispose();
        }
    }
}