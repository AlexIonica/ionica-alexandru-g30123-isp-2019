package Ionica.Alex.lab7.ex4;

import java.io.Serializable;

public class Car implements Serializable {
    public String model;
    public int price;

    public Car(String model, int price){
        this.model = model;
        this.price = price;
    }

    public boolean equals(Car car){
        if(this.model.equals(car.model)) return true;
        else return false;
    }
}