package Ionica.Alex.lab6.ex4;

public class Main {

    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        Word cuv = new Word("cuvant1");
        Definition def = new Definition("def1");
        Word cuv1 = new Word("cuvant2");
        Definition def1 = new Definition("def2");
        Word cuv2 = new Word("cuvant3");
        Definition def2 = new Definition("def3");
        d.addWord(cuv,def);
        d.addWord(cuv1,def1);
        d.addWord(cuv2,def2);

        d.getAllDefinition();
        d.getDefinition(cuv);
        d.getAllWords();
    }
}