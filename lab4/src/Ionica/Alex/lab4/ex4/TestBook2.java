package Ionica.Alex.lab4.ex4;


import Ionica.Alex.lab4.ex2.Author;

public class TestBook2 {
    public static void main(String[] args) {
        Author a1 = new Author("Author_1", "email@it.something", 'm');
        Author a2 = new Author("Author_2", "email@it.something", 'm');
        Author a3 = new Author("Author_3", "email@it.something", 'f');
        Author a4 = new Author("Author_4", "email@it.something", 'f');

        Book2 b1 = new Book2("Book_1", new Author[]{a1,a2}, 10000, 10);
        Book2 b2 = new Book2("Book_2",new Author[]{a1,a2,a3,a4},100,1);
        Book2 b3 = new Book2("Book_3",new Author[]{a2,a3},350);
        System.out.println("The quantity of "+ b1.getName() + " is " + b1.getQtyInStock());
        b1.setQtyInStock(5);
        System.out.println("Now the quantity of "+ b1.getName() + " is " + b1.getQtyInStock());
        b1.printAuthors();
        System.out.println("The price of "+ b3.getName() + " is " + b3.getPrice());
        b3.setPrice(250);
        System.out.println("Now the price of "+ b3.getName() + " is " + b3.getPrice());
        System.out.println(b2.toString());
        System.out.println(b2.getName());
    }
}