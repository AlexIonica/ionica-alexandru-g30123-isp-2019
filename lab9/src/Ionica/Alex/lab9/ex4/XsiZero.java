package Ionica.Alex.lab9.ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class XsiZero extends JFrame implements ActionListener {

    private JButton button1, button2, button3, button4, button5, button6, button7, button8, button9;
    private String rand = "X";

    public XsiZero() {
        initializare();
        setVisible(true);
    }

    private void initializare() {

        setLayout(new GridLayout(3, 3));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        button1 = new JButton();
        button2 = new JButton();
        button3 = new JButton();
        button4 = new JButton();
        button5 = new JButton();
        button6 = new JButton();
        button7 = new JButton();
        button8 = new JButton();
        button9 = new JButton();

        add(button1);
        add(button2);
        add(button3);
        add(button4);
        add(button5);
        add(button6);
        add(button7);
        add(button8);
        add(button9);

        pack();
        setSize(500, 500);

        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button4.addActionListener(this);
        button5.addActionListener(this);
        button6.addActionListener(this);
        button7.addActionListener(this);
        button8.addActionListener(this);
        button9.addActionListener(this);
    }

    private void checkWin() {
        boolean gameOver = false;
        if (button1.getText().equals(button2.getText()) && button2.getText().equals(button3.getText()) && button1.getText() != "" && button2.getText() != "" && button3.getText() != "") {
            new WinDialog(button1.getText());
            gameOver = true;
        }
        if (button4.getText().equals(button5.getText()) && button5.getText().equals(button6.getText()) && button4.getText() != "" && button5.getText() != "" && button6.getText() != "") {
            new WinDialog(button4.getText());
            gameOver = true;
        }
        if (button7.getText().equals(button8.getText()) && button8.getText().equals(button9.getText()) && button7.getText() != "" && button8.getText() != "" && button9.getText() != "") {
            new WinDialog(button7.getText());
            gameOver = true;
        }
        if (button1.getText().equals(button4.getText()) && button4.getText().equals(button7.getText()) && button1.getText() != "" && button4.getText() != "" && button7.getText() != "") {
            new WinDialog(button1.getText());
            gameOver = true;
        }
        if (button2.getText().equals(button5.getText()) && button5.getText().equals(button8.getText()) && button2.getText() != "" && button5.getText() != "" && button8.getText() != "") {
            new WinDialog(button2.getText());
            gameOver = true;
        }
        if (button3.getText().equals(button6.getText()) && button6.getText().equals(button9.getText()) && button3.getText() != "" && button6.getText() != "" && button9.getText() != "") {
            new WinDialog(button3.getText());
            gameOver = true;
        }
        if (button1.getText().equals(button5.getText()) && button5.getText().equals(button9.getText()) && button1.getText() != "" && button5.getText() != "" && button9.getText() != "") {
            new WinDialog(button5.getText());
            gameOver = true;
        }
        if (button3.getText().equals(button5.getText()) && button5.getText().equals(button7.getText()) && button3.getText() != "" && button5.getText() != "" && button7.getText() != "") {
            new WinDialog(button3.getText());
            gameOver = true;
        }

        if (gameOver == true) {
            button1.setEnabled(false);
            button2.setEnabled(false);
            button3.setEnabled(false);
            button4.setEnabled(false);
            button5.setEnabled(false);
            button6.setEnabled(false);
            button7.setEnabled(false);
            button8.setEnabled(false);
            button9.setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ((JButton) e.getSource()).setText(rand);
        ((JButton) e.getSource()).setEnabled(false);
        if (rand.equals("X")) rand = "0";
        else rand = "X";
        checkWin();
    }
}

class WinDialog extends JFrame implements ActionListener {

    JLabel wins;
    JButton ok;


    public WinDialog(String winner) {

        setTitle(winner + " is the winner!");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(250, 150);
        this.setLayout(null);

        wins = new JLabel(winner + " player wins.");
        wins.setBounds(80, 20, 80, 20);

        ok = new JButton("Ok");
        ok.setBounds(85, 50, 70, 30);
        ok.addActionListener(this);

        add(wins);
        add(ok);

        setVisible(true);
        setResizable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        dispose();
    }
}

class TestXsiZero {
    public static void main(String[] args) {
        new XsiZero();
    }
}