package Ionica.Alex.lab4.ex3;

import Ionica.Alex.lab4.ex2.Author;

public class Book extends Author {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, String email, char gender, String name1, Author author, double price) {
        super(name, email, gender);
        this.name = name1;
        this.author = author;
        this.price = price;
    }

    public Book(String name, String email, char gender, String name1, Author author, double price, int qtyInStock) {
        super(name, email, gender);
        this.name = name1;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        System.out.println("book-" + getName() + " by " + super.toString());
        return null;
    }
}

