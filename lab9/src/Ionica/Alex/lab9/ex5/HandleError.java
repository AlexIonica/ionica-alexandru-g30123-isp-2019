package Ionica.Alex.lab9.ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HandleError extends JFrame implements ActionListener {

    JLabel message;
    JButton Cancel, OK;

    public HandleError(int errCode) {
        action(errCode);
    }

    private void action(int errCode) {
        setTitle("Error");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLayout(null);

        Cancel = new JButton("Cancel");
        Cancel.setBounds(100, 100, 80, 40);

        OK = new JButton("OK");
        OK.setBounds(200, 100, 80, 40);

        Cancel.addActionListener(this);
        OK.addActionListener(this);

        add(OK);
        add(Cancel);

        setSize(400, 200);
        setResizable(false);
        setVisible(true);

        if (errCode == 1) {
            //#1 error: Train station name is null
            Cancel.setEnabled(false);
            message = new JLabel("Numele statiei nu poate sa fie null!");
            SimulatorGUI.logContentSetter("!!!Error: Numele statiei nu poate sa fie null!!!");
        }

        if (errCode == 2) {
            //#2 error: Segment id is null or contains no integer
            Cancel.setEnabled(false);
            message = new JLabel("Segmentele trebuie sa aiba un ID!");
            SimulatorGUI.logContentSetter("!!!Error: Segmentele trebuie sa aiba un ID!!!");
        }

        if (errCode == 3) {
            //#3 error: There are no controllers created to add a train
            Cancel.setEnabled(false);
            message = new JLabel("Nu exista statii create!");
            SimulatorGUI.logContentSetter("!!!Error: Nu exista statii create!!!");
        }

        if (errCode == 4) {
            //#4 error: The train has no name
            Cancel.setEnabled(false);
            message = new JLabel("Numele trenului nu poate fi null!");
            SimulatorGUI.logContentSetter("!!!Error: Numele trenului nu poate fi null!!!");
        }

        message.setBounds(100, 50, 300, 20);
        add(message);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (((JButton) e.getSource()).getText().equals("OK")) dispose();
        if (((JButton) e.getSource()).getText().equals("Cancel")) System.exit(1);
    }
}