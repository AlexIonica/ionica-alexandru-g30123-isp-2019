package Ionica.Alex.lab10.ex4;


import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Robot extends Thread {

    String nume;
    int PosX;
    int PosY;

    public Robot(String nume, int PosX, int PosY) {
        super(nume);
        this.nume = nume;
        this.PosX = PosX;
        this.PosY = PosY;
    }

    public void run() {
        while (true) {

            PosX = ThreadLocalRandom.current().nextInt(0,100);
            PosY = ThreadLocalRandom.current().nextInt(0,100);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(nume+"Position:"+PosX+"."+PosY);
            Plansa.checkCollision();
        }
    }
}

