package Ionica.Alex.lab2.ex6a;

import java.util.Scanner;

public class Nonrecursive {
    public static void main(String args[]){
        int i,fact=1;
        Scanner  num = new Scanner(System.in);
        System.out.println("Which is the number?");
        int N = num.nextInt();
        for(i=1;i<=N;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+N+" is: "+fact);
    }
}