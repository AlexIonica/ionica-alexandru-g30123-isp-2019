package Ionica.Alex.lab2.ex6b;

import java.util.Scanner;

public class Recursive {
    static int factorial(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorial(n - 1));
    }

    public static void main(String args[]) {
        int i, fact = 1;
        Scanner num = new Scanner(System.in);
        System.out.println("Which is the number?");
        int N = num.nextInt();
        fact = factorial(N);
        System.out.println("Factorial of " + N + " is: " + fact);
    }
}