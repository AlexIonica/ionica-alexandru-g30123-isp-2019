package Ionica.Alex.lab6.ex1;

public class TestBanckAccount {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Popescu", 2000);
        BankAccount b2 = new BankAccount("Ionescu", 2000);

        b1.deposit(50);
        b2.withdraw(100);
        if (b1.hashCode() == b2.hashCode()) {
            if (b1.equals(b2))
                System.out.println("Same");
            else
                System.out.println("Not the same");
        } else {
            System.out.println("Not the same(hash)");
        }
    }
}
