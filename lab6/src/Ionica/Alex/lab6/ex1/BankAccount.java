package Ionica.Alex.lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    void withdraw(double amount)
    {
        if (balance>0)
        {
            balance=balance-amount;
            System.out.println("New balance:"+balance);
        }
        else System.out.println("Out of money");
    }

    void deposit(double amount)
    {
        balance=balance+amount;
        System.out.println("New balance:"+balance);
    }


    @Override
    public boolean equals(Object o){
        if (o==this) return true;
        if (!(o instanceof BankAccount)){
            return false;
        }
        BankAccount ba = (BankAccount) o;

        return ba.owner.equals(owner) &&
                ba.balance == balance;
    }
    @Override
    public int hashCode(){
        return Objects.hash(owner,balance);
    }

}