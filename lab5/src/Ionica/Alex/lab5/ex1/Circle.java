package Ionica.Alex.lab5.ex1;

import static java.lang.Math.*;

public class Circle extends Shape {
    protected double radius;

    public Circle(){
    }

    public Circle(double radius){
        this.radius=radius;
    }

    public Circle(double radius, String color, boolean filled){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return PI*pow(this.radius,2);
    }

    @Override
    public double getPerimeter(){
        return PI*this.radius*2;
    }

    @Override
    public String toString(){
        String str = "A circle with an area of:"+getArea()+" and a perimeter of:"+getPerimeter()+" is filled:"+super.isFilled()+" and is colored:"+super.getColor();
        return str;
    }
}