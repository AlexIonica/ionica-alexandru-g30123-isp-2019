package Ionica.Alex.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(4, 4);
        p1.setXY(4, 4);
        p2.setX(10);
        p2.setY(10);
        System.out.println(p1.distance(2, 2));
        System.out.println(p1.distance(p2));
    }
}
