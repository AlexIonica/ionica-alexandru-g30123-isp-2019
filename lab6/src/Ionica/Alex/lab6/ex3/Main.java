package Ionica.Alex.lab6.ex3;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        TreeSet<BankAccount> bankaccounts = new TreeSet<>();
        Bank b= new Bank();
        b.addAccount("Alex",60);
        b.addAccount("Mihai", 170);
        b.addAccount("Andrei", 400);
        b.addAccount("Ionut", 150);


        b.printAccounts();

        b.printAccounts(150, 300);

    }
}
