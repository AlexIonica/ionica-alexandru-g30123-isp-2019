package Ionica.Alex.lab9.ex5;

import java.util.*;

//public class Simulator {
//
//    /**
//     * @param args
//     */
//    public static void main(String[] args) {
//
//        //build station Cluj-Napoca
//        Controler c1 = new Controler("Cluj-Napoca");
//
//        Segment s1 = new Segment(1);
//        Segment s2 = new Segment(2);
//        Segment s3 = new Segment(3);
//
//        c1.addControlledSegment(s1);
//        c1.addControlledSegment(s2);
//        c1.addControlledSegment(s3);
//
//        //build station Bucuresti
//        Controler c2 = new Controler("Bucuresti");
//
//        Segment s4 = new Segment(4);
//        Segment s5 = new Segment(5);
//        Segment s6 = new Segment(6);
//
//        c2.addControlledSegment(s4);
//        c2.addControlledSegment(s5);
//        c2.addControlledSegment(s6);
//
//        //connect the 2 controllers
//
//        c1.setNeighbourController(c2);
//        c2.setNeighbourController(c1);
////new code
//
//        System.out.println("new code test");
//        Controler c3 = new Controler("Test");
//
//        Segment s7 = new Segment(7);
//        Segment s8 = new Segment(8);
//        Segment s9 = new Segment(9);
//
//        c3.addControlledSegment(s7);
//        c3.addControlledSegment(s8);
//        c3.addControlledSegment(s9);
//
//        c1.setNeighbourController(c3);
//        c3.setNeighbourController(c1);
//        c2.setNeighbourController(c3);
//        c3.setNeighbourController(c2);
//
//        //testing
//
//        Train t1 = new Train("Bucuresti", "IC-001");
//        s1.arriveTrain(t1);
//
//        Train t2 = new Train("Cluj-Napoca","R-002");
//        s5.arriveTrain(t2);
//
//        Train t3 = new Train("Cluj-Napoca","R-test");
//        s8.arriveTrain(t3);
//
//
//        c1.displayStationState();
//        c2.displayStationState();
//        c3.displayStationState();
//
//        System.out.println("\nStart train control\n");
//
//        //execute 3 times controller steps
//        for(int i = 0;i<3;i++){
//            System.out.println("### Step "+i+" ###");
//            c1.controlStep();
//            c2.controlStep();
//            c3.controlStep();
//
//            System.out.println();
//
//            c1.displayStationState();
//            c2.displayStationState();
//            c3.displayStationState();
//        }
//    }
//
//}

class Controler {

    String stationName;

    ArrayList<Controler> neighbourControllerList = new ArrayList<Controler>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v) {
        neighbourControllerList.add(v);
    }

    void addControlledSegment(Segment s) {
        list.add(s);
    }

    public ArrayList<Segment> getListOfSegments() {
        return list;
    }

    public ArrayList<Controler> getNeighbourControllerList() {
        return neighbourControllerList;
    }

    boolean isNeighbour(Controler c) {
        for (int i = 0; i < neighbourControllerList.size(); i++) {
            if (neighbourControllerList.get(i).equals(c)) return true;
        }
        return false;
    }

    void removeNeighbourController(Controler v) {
        for (int i = 0; i < neighbourControllerList.size(); i++) {
            if (v.stationName.equals(neighbourControllerList.get(i).stationName)) {
                neighbourControllerList.remove(i);
                return;
            }
        }
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId() {
        for (Segment s : list) {
            if (s.hasTrain() == false)
                return s.id;
        }
        return -1;
    }

    void controlStep() {
        //check which train must be sent
        for (int i = 0; i < neighbourControllerList.size(); i++) {
            for (Segment segment : list) {
                if (segment.hasTrain()) {
                    Train t = segment.getTrain();

                    if (t.getDestination().equals(neighbourControllerList.get(i).stationName)) {
                        //check if there is a free segment
                        int id = neighbourControllerList.get(i).getFreeSegmentId();
                        if (id == -1) {
                            SimulatorGUI.logContentSetter("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + neighbourControllerList.get(i).stationName + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        SimulatorGUI.logContentSetter("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + neighbourControllerList.get(i).stationName);
                        segment.departTRain();
                        neighbourControllerList.get(i).arriveTrain(t, id);
                    }

                }
            }//.for

        }//.
    }

    public void arriveTrain(Train t, int idSegment) {
        for (Segment segment : list) {
            //search id segment and add train on it
            if (segment.id == idSegment)
                if (segment.hasTrain() == true) {
                    SimulatorGUI.logContentSetter("CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                    return;
                } else {
                    SimulatorGUI.logContentSetter("Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        SimulatorGUI.logContentSetter("Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

    }


    public String displayStationState() {
        String string = new String();
        string += "=== STATION " + stationName + " ===\n";
        for (Segment s : list) {
            if (s.hasTrain())
                string += "|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|\n";
            else
                string += "|----------ID=" + s.id + "__Train=______ catre ________----------|\n";
        }
        return string;
    }

    @Override
    public String toString() {
        return this.stationName;
    }
}


class Segment {
    int id;
    Train train;

    Segment(int id) {
        this.id = id;
    }

    boolean hasTrain() {
        return train != null;
    }

    Train departTRain() {
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t) {
        train = t;
    }

    Train getTrain() {
        return train;
    }
}

class Train {
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination() {
        return destination;
    }

}