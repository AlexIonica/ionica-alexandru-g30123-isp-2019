package Ionica.Alex.lab6.ex2;

import java.util.Comparator;


public class BankAccount implements Comparable <BankAccount>{
    private String owner;
    private double balance;

    BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    String getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return "BankAccount [owner=" + owner + ", balance=" + balance + "]";
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    void withdraw(double amount)
    {
        if (balance>amount)
        {
            balance=balance-amount;
            System.out.println("New balance:"+balance);
        }
        else System.out.println("Out of money");
    }

    void deposit(double amount)
    {
        balance=balance+amount;
        System.out.println("New balance:"+balance);
    }

    @Override
    public int compareTo(BankAccount other) {

        int CompareInt=this.owner.compareTo(other.owner);
        return Integer.compare(CompareInt, 0);

    }
}
