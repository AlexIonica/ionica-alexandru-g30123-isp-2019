package Ionica.Alex.lab9.ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class AddTrainGUI extends JFrame implements ActionListener, ItemListener {

    JTextField numeTren;
    JLabel nume, statia, segment, destinatia;
    JButton Cancel, OK;
    Controler cont = SimulatorGUI.Controleri.get(0);
    JComboBox destinatii, statii, segmente;
    ArrayList<Segment> stationSegments = new ArrayList<Segment>();
    ArrayList<Controler> sosiri = new ArrayList<Controler>();

    private String setDestination;
    private Segment setSegmentID;

    public AddTrainGUI() {
        setTitle("Add a train.");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 200);
        setResizable(false);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);

        stationSegments = cont.getListOfSegments();

        nume = new JLabel("Introduceti numele:");
        nume.setBounds(30, 10, 120, 20);

        numeTren = new JTextField();
        numeTren.setBounds(150, 10, 200, 20);

        statia = new JLabel("Selectati statia:");
        statia.setBounds(50, 40, 200, 20);

        statii = new JComboBox();
        for (int i = 0; i < SimulatorGUI.Controleri.size(); i++) {
            statii.addItem(SimulatorGUI.Controleri.get(i).stationName);
        }
        statii.setSelectedIndex(0);
        statii.setBounds(150, 40, 200, 20);
        statii.addItemListener(this);

        segment = new JLabel("ID Segment:");
        segment.setBounds(70, 70, 200, 20);

        destinatia = new JLabel("Selectati destinatia:");
        destinatia.setBounds(25, 100, 200, 20);

        Cancel = new JButton("Cancel");
        Cancel.setBounds(180, 130, 80, 20);

        OK = new JButton("OK");
        OK.setBounds(270, 130, 80, 20);

        Cancel.addActionListener(this);
        OK.addActionListener(this);

        segmente = new JComboBox();
        segmente.setBounds(150, 70, 200, 20);
        segmente.addItemListener(this);

        for (int i = 0; i < stationSegments.size(); i++) {
            segmente.addItem(stationSegments.get(i).id);
        }

        destinatii = new JComboBox();
        destinatii.setBounds(150, 100, 200, 20);
        for (int i = 0; i < SimulatorGUI.Controleri.size(); i++) {
            if (SimulatorGUI.Controleri.get(0).isNeighbour(SimulatorGUI.Controleri.get(i)))
                destinatii.addItem(SimulatorGUI.Controleri.get(i));
        }
        destinatii.addItemListener(this);
        setDestination = String.valueOf(destinatii.getItemAt(0));
        setSegmentID = stationSegments.get(0);

        add(destinatii);
        add(OK);
        add(Cancel);
        add(nume);
        add(numeTren);
        add(statia);
        add(statii);
        add(segmente);
        add(destinatia);
        add(segment);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (((JButton) e.getSource()).getText().equals("OK"))
            if (numeTren.getText().equals("")) {
                new HandleError(4);
            } else {
                Train tren = new Train(setDestination, numeTren.getText());
                setSegmentID.arriveTrain(tren);
                SimulatorGUI.Trenuri.add(tren);
                SimulatorGUI.logContentSetter("Tren creat:" + numeTren.getText() + " pe segmentul:" + setSegmentID.id + " catre:" + setDestination);
                dispose();
            }
        if (((JButton) e.getSource()).getText().equals("Cancel")) dispose();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource().equals(statii)) {
            cont = SimulatorGUI.getController(String.valueOf(statii.getSelectedItem()));
            stationSegments = cont.getListOfSegments();
            sosiri = cont.getNeighbourControllerList();
            segmente.removeAllItems();
            destinatii.removeAllItems();
            for (int i = 0; i < stationSegments.size(); i++) {
                segmente.addItem(stationSegments.get(i).id);
            }

            for (int i = 0; i < sosiri.size(); i++) {
                destinatii.addItem(sosiri.get(i).stationName);
            }
            segmente.setSelectedIndex(0);
            destinatii.setSelectedIndex(0);
        }

        if (e.getSource().equals(segmente)) {
            if (segmente.getSelectedItem() != null)
                setSegmentID = SimulatorGUI.getSegment(Integer.valueOf((Integer) segmente.getSelectedItem()));
        }

        if (e.getSource().equals(destinatii)) {
            if (destinatii.getSelectedItem() != null) setDestination = String.valueOf(destinatii.getSelectedItem());
        }
    }
}