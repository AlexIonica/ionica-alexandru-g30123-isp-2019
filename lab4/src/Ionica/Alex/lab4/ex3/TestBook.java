package Ionica.Alex.lab4.ex3;

import Ionica.Alex.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author A = new Author("Alex", "alex.ionica@titeica.com", 'm');
        Book B1 = new Book("Mihai","mihai@gmail.com",'m',"The Book",A,300);
        Book B2 = new Book("Andrei","andrei@gmail.com",'m',"Cars",A,370,5);
        System.out.println("The author of B1 is "+ B1.getAuthor());
        System.out.println("The price of the book 'The Book' is "+ B1.getPrice());
        B1.setPrice(555);
        System.out.println("The new price of the book 'The Book' is "+ B1.getPrice());
        System.out.println("The quantity of the book 'Cars' is "+ B2.getQtyInStock());
    }
}
