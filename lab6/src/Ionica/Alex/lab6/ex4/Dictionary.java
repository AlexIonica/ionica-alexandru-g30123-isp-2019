package Ionica.Alex.lab6.ex4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class Dictionary {

    private HashMap<Word,Definition> h = new HashMap<>();

    void addWord(Word w, Definition d)
    {
        h.put(w, d);
    }

    Definition getDefinition(Word w)
    {
        if(h.containsKey(w)) {
            System.out.println("\n"+h.get(w).getDescription());
            return h.get(w);
        }
        System.out.println("Cuvantul cautat nu a fost gasit!");


        return null;
    }

    void getAllWords()
    {
        Set a = h.entrySet();
        Iterator iterator= a.iterator();
        System.out.println("\n");
        while(iterator.hasNext())
        {
            Map.Entry<Word,Definition> intmap = (Map.Entry<Word,Definition>)iterator.next();
            System.out.println("\n"+intmap.getKey().getName());
        }
    }

    void getAllDefinition()
    {
        Set a = h.entrySet();
        Iterator iterator= a.iterator();
        System.out.println("\n");
        while(iterator.hasNext())
        {
            Map.Entry<Word,Definition> intmap = (Map.Entry<Word,Definition>)iterator.next();
            System.out.println("\n"+intmap.getValue().getDescription());
        }

    }


}