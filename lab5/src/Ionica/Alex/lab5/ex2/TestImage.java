package Ionica.Alex.lab5.ex2;


public class TestImage {
    public static void main(String[] args){
        RealImage rImg = new RealImage("Fisier");
        rImg.display();
        ProxyImage pImg = new ProxyImage("PFisier");
        pImg.display();
        Image test = new RealImage("test");
        test.display();

        ProxyImage test2 = new ProxyImage("test2");
        test2.display();

        test2 = new ProxyImage("test2",1);
        test2.display();

        RotatedImage test3 = new RotatedImage("test3");
        test3.display();
    }
}