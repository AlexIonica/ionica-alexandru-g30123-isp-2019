package Ionica.Alex.lab8.ex4;

import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class ControlUnit {

    public static int threshold;
    private FireSensor[] fireSensors = new FireSensor[2];
    private static ControlUnit instance = null;

    ControlUnit(int thres) {

        threshold = thres;

        TemperatureSensor ts = new TemperatureSensor(threshold);
        fireSensors[0] = new FireSensor();
        fireSensors[1] = new FireSensor();
        //ambii senzori vor fi porniti la detectarea unui singur event de foc.
        //pentru functionare separata mai trebuie creat un event de fire (de ex pt o camera separata)

        Home h = new Home() {
            protected void setValueInEnvironment(Event event) {
                System.out.println("New event in environment " + event);
                ts.tempWatchdog(event);
                fireSensors[0].fireWatchdog(event);
                fireSensors[1].fireWatchdog(event);
            }

            protected void controllStep() {
                System.out.println("Control step executed");
            }
        };
        h.simulate();
    }

    public static ControlUnit getInstance(int threshold) {
        if (instance == null) instance = new ControlUnit(threshold);
        return instance;
    }
}