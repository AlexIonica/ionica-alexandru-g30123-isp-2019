package Ionica.Alex.lab7.ex3;

import java.io.*;

public class Decrypt {
    public static void action(FileReader file, String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(file);
        String input = reader.readLine();
        System.out.println(input);

        char[] stringToCharArr = input.toCharArray();
        String outString = "";

        for(char crt:stringToCharArr){
            outString += String.valueOf(--crt);
        }

        System.out.println(outString);

        PrintWriter out = new PrintWriter("dataIn/"+fileName+".dec");
        out.println(outString);
        out.close();
    }
}