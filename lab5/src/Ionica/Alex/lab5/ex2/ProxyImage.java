package Ionica.Alex.lab5.ex2;

public class ProxyImage implements Image {

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private int arg = 0;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    public ProxyImage(String fileName, int arg) {
        this.fileName = fileName;
        this.arg = arg;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
            realImage.display();
        }
        if (rotatedImage == null) {
            rotatedImage = new RotatedImage(fileName);
            rotatedImage.display();
        }
    }
}