package Ionica.Alex.lab7.ex3;


import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Test{
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);

        System.out.println("Introduceti numele fisierului pt encriptat:");
        String fileName = scan.nextLine();

        FileReader file = new FileReader("dataIn/"+fileName+".dec");
        Encrypt.action(file,fileName);

        System.out.println("Introduceti numele fisierului pt decriptat:");
        fileName = scan.nextLine();

        FileReader file2 = new FileReader("dataIn/"+fileName+".enc");
        Decrypt.action(file2,fileName);
    }
}