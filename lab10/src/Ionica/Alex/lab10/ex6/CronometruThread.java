package Ionica.Alex.lab10.ex6;

public class CronometruThread extends Thread{

    private static int msec = 0,
            sec = 0,
            min = 0,
            ora = 0;
    private boolean running = false;

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void reset(){
        msec = 0;
        sec = 0;
        min = 0;
        ora = 0;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msec++;
            if (msec == 1000) {
                msec = 0;
                sec++;
            }
            if (sec == 60) {
                sec = 0;
                min++;
            }
            if (min == 60) {
                min = 0;
                ora++;
            }
            CronometruGUI.writeTime(msec, sec, min, ora);
        }
    }
}