package Ionica.Alex.lab7.ex4;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CarPark implements Serializable {
    public String nume;
    List<Car> park = new ArrayList<Car>();

    public CarPark(String nume) {
        this.nume = nume;
    }

    public void add(Car car) {
        this.park.add(car);
    }

    public void viewCars() {
        int cnt = 0;
        for (Car car : park) {
            System.out.println(++cnt + ". model:" + car.model + ", pret:" + car.price);
        }
    }

    public void searchCar(Car carS){
        for (Car car : park) {
            if(car.equals(carS)) System.out.println("Model:" + car.model + ", pret:" + car.price);
        }
    }

    public void save(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Grupul a fost salvat in fisierul:" + fileName);
        } catch (IOException e) {
            System.err.println("Grupul nu poate fi salvat in fisier.");
            e.printStackTrace();
        }
    }

    public static CarPark read(String fileName) {
        CarPark cp = null;
        try {
            ObjectInputStream o = new ObjectInputStream(new FileInputStream(fileName));
            cp = (CarPark) o.readObject();
            System.out.println("Grupul a fost citit din fisierul:" + fileName);
        } catch (IOException e) {
            System.err.println("Grupul nu poate fi citit din fisier.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return cp;
    }
}