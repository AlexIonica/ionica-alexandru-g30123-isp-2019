package Ionica.Alex.lab8.ex4;

import static java.lang.Integer.parseInt;

public class TemperatureSensor {
    private static int thres;

    public TemperatureSensor(int threshold) {
        thres = threshold;
    }

    public void tempWatchdog(Event s) {
        if (s.toString().contains("NoEvent") == true || s.toString().contains("FireEvent") == true) {
        } else {
            if (parseInt(s.toString().replaceAll("\\D+", "")) < thres)
                HeatingUnit.turnOn(parseInt(s.toString().replaceAll("\\D+", "")), thres);
            else if (parseInt(s.toString().replaceAll("\\D+", "")) > thres)
                CoolingUnit.turnOn(parseInt(s.toString().replaceAll("\\D+", "")), thres);
            else if (parseInt(s.toString().replaceAll("\\D+", "")) == thres)
                System.out.println("Temperature is at set threshold");
        }
    }
}