package Ionica.Alex.lab6.ex3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private int balance;

    BankAccount(String owner, int balance)  {

        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount)
    {
        balance-=amount;
    }

    public void deposit(double amount)
    {
        balance+=amount;
    }

    String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public int compareTo(BankAccount o)
    {
        return Integer.compare(balance, o.getBalance());
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }


}