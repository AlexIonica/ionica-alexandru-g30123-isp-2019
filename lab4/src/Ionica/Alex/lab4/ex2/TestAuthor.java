package Ionica.Alex.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args) {
        Author A = new Author("Alex", "alex.ionica@titeica.com", 'm');
        System.out.println("The name of the author is " + A.getName());
        System.out.println("The email of the author is " + A.getEmail());
        System.out.println("The gender of the author is " + A.getGender());
        System.out.println(A.toString());
    }
}
