package Ionica.Alex.lab7.ex2;

import java.io.*;
import java.util.*;

public class CharacterCounter {
    public static void main(String args[]) throws IOException {
        char character;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Give the character");
        character = stdin.readLine().charAt(0);

        int number = 0;


        //citire fisier
        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Laborator 308\\ionica-alexandru-g30123-isp-2019\\lab7\\src\\Ionica\\Alex\\lab7\\ex2\\words"));
        String s = new String();
        while ((s = in.readLine()) != null) {
            char lin[] = s.toCharArray();
            for (char c : lin) {
                if (c == character) {
                    number++;
                }
            }
        }
        in.close();
        System.out.println("The character " + character + " appears " + number + " times");
    }
}

