package Ionica.Alex.lab7.ex4;

import java.util.Scanner;

public class Test {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.println("Introduceti numele grupului de masini:");
        String numeParcare = scan.nextLine();
        CarPark cp = new CarPark(numeParcare);

        Car c1 = new Car("bmw", 1000);
        Car c2 = new Car("merc", 2000);
        Car c3 = new Car("dacia", 1);

        cp.add(c1);
        cp.add(c2);
        cp.add(c3);

        cp.viewCars();
        cp.save("dataIn/"+numeParcare);

        CarPark cp2 = CarPark.read("dataIn/"+numeParcare);
        cp2.viewCars();
        cp2.searchCar(c1);
    }
}