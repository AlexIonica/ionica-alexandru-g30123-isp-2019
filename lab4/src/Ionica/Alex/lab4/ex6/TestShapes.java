package Ionica.Alex.lab4.ex6;

public class TestShapes {
    public static void main(String[] args){
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(2.0,3.0);
        Rectangle r3 = new Rectangle("purple",true,2.0,3.0);
        System.out.println("The width of r1 is " + r1.getWidth());
        r1.setWidth(2.5);
        System.out.println("Now the width of r1 is " + r1.getWidth());
        System.out.println("The length of r2 is " + r2.getLength());
        r2.setLength(3.5);
        System.out.println("Now the length of r2 is " + r2.getLength());
        System.out.println("The area of r3 is " + r3.getArea());
        System.out.println("The perimeter of r3 is " + r3.getPerimeter());
        r3.toString();

        Circle c1 = new Circle();
        Circle c2 = new Circle(2.0);
        Circle c3 = new Circle("yellow",true,2.5);
        Circle c4 = new Circle("black",false,3.5);
        System.out.println("The radius of c1 is " + c1.getRadius());
        System.out.println("The radius of c3 is " + c3.getRadius());
        System.out.println("The area of c3 is " + c3.getArea());
        c3.toString();
        System.out.println("The color of c2 is " + c2.getColor());
        c2.setColor("blue");
        System.out.println("Now the color of c2 is " + c2.getColor());
        System.out.println("The color of c4 is " + c4.getColor());
        System.out.println("The circle c4 is "+ c4.isFilled());
        c4.setFilled(true);
        System.out.println("Now the circle c4 is "+ c4.isFilled());
        c4.toString();

        Square s1 = new Square();
        Shape shape1 = new Shape();
        if(r1.isFilled())
            System.out.println(r1.toString());
    }
}
