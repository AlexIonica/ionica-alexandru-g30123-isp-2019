package Ionica.Alex.lab3.ex1;

public class Robot {
    int x;

    public Robot() {
        x = 1;
    }

    public void change(int k) {
        if (k >= 1)
            this.x = this.x + k;
    }

    public String toString() {
        return "The current value is:" + this.x;
    }
}

