package Ionica.Alex.lab6.ex3;

import java.util.*;

public class Bank {
    private TreeSet<BankAccount> bankaccounts;

    Bank() {
        bankaccounts = new TreeSet<>();
    }

    public Bank(TreeSet<BankAccount> bankaccounts) {
        this.bankaccounts = bankaccounts;
    }

    void addAccount(String owner, int balance) {
        bankaccounts.add(new BankAccount(owner, balance));
    }

    public BankAccount getAccount(String owner) {
        Iterator<BankAccount> iterator = bankaccounts.iterator();
        BankAccount account;
        while (iterator.hasNext()) {
            account = iterator.next();
            if (account.getOwner().equals(owner))
                return account;
        }
        return null;
    }

    void printAccounts() {
        Set<BankAccount> BankAccountSet = new TreeSet<>();

        Iterator<BankAccount> iterator = bankaccounts.iterator();
        BankAccount account;
        while (iterator.hasNext()) {
            account = iterator.next();
            System.out.println(account.getOwner() + " " + account.getBalance());
        }
    }

    void printAccounts(double minBalance, double maxBalance) {
        Set<BankAccount> BankAccountSet = new TreeSet<>();
        Iterator<BankAccount> iterator = bankaccounts.iterator();
        BankAccount bankAccount;
        while (iterator.hasNext()) {
            bankAccount = iterator.next();
            if (bankAccount.getBalance() <= maxBalance && bankAccount.getBalance() >= minBalance) {
                System.out.println(bankAccount.getOwner() + " " + bankAccount.getBalance());

            }
        }
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return bankaccounts;
    }
}