package Ionica.Alex.lab9.ex5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class SetNeighbours extends JFrame implements ActionListener, ItemListener {

    JPanel panouSelectie;
    private JLabel selectTargetL, vecini;
    private JComboBox selectTargetCombo;
    private JButton operationDone;
    private List<Controler> controllers = SimulatorGUI.Controleri;
    private List<JCheckBox> stationCheck = new ArrayList<JCheckBox>();
    private int nrControlleri = SimulatorGUI.Controleri.size();
    private Controler target, neighbour;

    public SetNeighbours() {
        setTitle("Set Neighbours");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        setSize(440, 120);
        setResizable(false);
        setVisible(true);

        selectTargetL = new JLabel("Selectati Statia la care doriti sa adaugati vecini: ");
        selectTargetL.setBounds(10, 10, 400, 20);
        vecini = new JLabel("Selectati vecinii: ");
        vecini.setBounds(10, 80, 120, 20);

        selectTargetCombo = new JComboBox();
        for (int i = 0; i < controllers.size(); i++) {
            selectTargetCombo.addItem(controllers.get(i).stationName);
        }
        selectTargetCombo.setBounds(10, 40, 400, 30);
        selectTargetCombo.addItemListener(this);

        operationDone = new JButton(new AbstractAction("Done") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        operationDone.setBounds(300, 10, 100, 20);


        add(selectTargetCombo);
        add(selectTargetL);
        add(vecini);
        add(operationDone);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("testCheck");
        for (int i = 0; i < stationCheck.size(); i++) {
            if (stationCheck.get(i).isSelected()) {
                System.out.println("testTrue");
                target = SimulatorGUI.getController(String.valueOf(selectTargetCombo.getSelectedItem()));
                neighbour = SimulatorGUI.getController(String.valueOf(stationCheck.get(i).getText()));
                if (target.isNeighbour(neighbour)) {
                    //do nothing
                } else {
                    target.setNeighbourController(neighbour);
                    neighbour.setNeighbourController(target);
                }
            } else {
                System.out.println("testFalse");
                target = SimulatorGUI.getController(String.valueOf(selectTargetCombo.getSelectedItem()));
                neighbour = SimulatorGUI.getController(String.valueOf(stationCheck.get(i).getText()));
                target.removeNeighbourController(neighbour);
                neighbour.removeNeighbourController(target);
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        Object sursa = e.getSource();
        if (sursa.equals(selectTargetCombo)) {
            setSize(440, 170 + nrControlleri * 25);


            if (stationCheck.size() != 0) {
                remove(panouSelectie);
                revalidate();
                repaint();
                stationCheck.removeAll(stationCheck);
            }

            panouSelectie = new JPanel();
            int counter = 0;
            for (int i = 0; i < nrControlleri; i++) {
                if (String.valueOf(controllers.get(i).stationName).equals(String.valueOf(selectTargetCombo.getSelectedItem()))) {
                    //do nothing
                } else {
                    stationCheck.add(new JCheckBox(String.valueOf(controllers.get(i).stationName)));
                    stationCheck.get(counter).setBounds(10, 10 + counter * 25, 100, 20);
                    if (controllers.get(i).isNeighbour(SimulatorGUI.getController(String.valueOf(selectTargetCombo.getSelectedItem())))) {
                        stationCheck.get(counter).setSelected(true);
                    } else {
                        stationCheck.get(counter).setSelected(false);
                    }
                    stationCheck.get(counter).addActionListener(this);
                    panouSelectie.add(stationCheck.get(counter));
                    counter++;
                }
            }
            panouSelectie.setBorder(BorderFactory.createLineBorder(Color.black));
            panouSelectie.setLocation(10, 120);
            panouSelectie.setSize(400, nrControlleri * 25 - 10);
            panouSelectie.setLayout(null);
            add(panouSelectie);
        }

    }
}