package Ionica.Alex.lab5.ex4;

import Ionica.Alex.lab5.ex3.LightSensor;
import Ionica.Alex.lab5.ex3.TemperatureSensor;

import java.util.concurrent.TimeUnit;

class Controller2 {
    private static Controller2 single_instance = null;

    private Controller2() {
    }

    public void control2() throws InterruptedException {
        short count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:" + TemperatureSensor.getTempSensor() + " si LightSensor:" + LightSensor.getKightSensor());
            count++;
        }
    }

    public static Controller2 getInstance() {
        if (single_instance == null) single_instance = new Controller2();
        return single_instance;
    }

}

class Main {
    public static void main(String[] args) throws InterruptedException {
        Controller2 c2 = Controller2.getInstance();
    }
}