package Ionica.Alex.lab4.ex5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder C = new Cylinder(5, 2);
        System.out.println("The radius of the cylinder is: " + C.getRadius());
        System.out.println("The height of the cylinder is " + C.getHeight());
        System.out.println("The area of the cylinder is " + C.getArea());
        System.out.println("The volume of the cylinder is " + C.getVolume());
        System.out.println(C.toString());

    }
}
