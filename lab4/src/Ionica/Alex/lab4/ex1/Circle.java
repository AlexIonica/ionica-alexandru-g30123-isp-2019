package Ionica.Alex.lab4.ex1;



public class Circle {
    private double radius = 1.0;
    private String color = "red";
    private static final float PI = 3.1415f;

    public Circle() {
        //   this.radius = 1.0;
        //   this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }
}